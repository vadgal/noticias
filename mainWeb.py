from flask import Flask, render_template, request, session, redirect, url_for
from flask_sqlalchemy import SQLAlchemy
from sqlalchemy.orm import relationship
from datetime import datetime
from flask_mail import Mail
from werkzeug import secure_filename
import os
import json
import math
from alphabet_detector import AlphabetDetector

local_server = True
with open('config.json', 'r') as f:
    parameter = json.load(f)["para"]

app = Flask(__name__)
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
# app.config['SQLALCHEMY_ECHO'] = True
app.secret_key = 'super-secret-key'
app.config['UPLOAD_FOLDER'] = parameter['upload_location']
app.config.update(
    MAIL_SERVER='smtp.gmail.com',
    MAIL_PORT='465',
    MAIL_USE_SSL='True',
    MAIL_USERNAME=parameter['gmail-user'],
    MAIL_PASSWORD=parameter['gmail-pass']
)
mail = Mail(app)
if local_server:
    app.config['SQLALCHEMY_DATABASE_URI'] = parameter['local_uri']
else:
    app.config['SQLALCHEMY_DATABASE_URI'] = parameter['prod_uri']
db = SQLAlchemy(app)


class Contacts(db.Model):
    # serial_no	name	phone_num	msg	date	email
    serial_no = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(50), nullable=False)
    phone_num = db.Column(db.String(14), nullable=False)
    msg = db.Column(db.String(200), nullable=False)
    date = db.Column(db.String(12), nullable=True)
    email = db.Column(db.String(50), nullable=False)


class Posts(db.Model):
    # serial_no	title slug content date
    serial_no = db.Column(db.Integer, primary_key=True)
    title = db.Column(db.String(50), nullable=False)
    tagline = db.Column(db.String(50), nullable=False)
    slug = db.Column(db.String(14), nullable=False)
    content = db.Column(db.String(50), nullable=False)
    date = db.Column(db.String(12), nullable=True)
    img_file = db.Column(db.String(30), nullable=True)
    author = db.Column(db.String(50), nullable=False)
    comments = db.relationship("Comments", backref=db.backref('posts', uselist=False))


class Comments(db.Model):
    # serial_no	login text date
    serial_no = db.Column(db.Integer, primary_key=True, autoincrement=True)
    login = db.Column(db.String(50), nullable=False)
    text = db.Column(db.String(500), nullable=False)
    date = db.Column(db.String(12), nullable=True)
    post_id = db.Column(db.Integer, db.ForeignKey('posts.serial_no', ondelete='CASCADE'), nullable=False)


class Users(db.Model):
    # serial_no	login text date
    serial_no = db.Column(db.Integer, primary_key=True)
    username = db.Column(db.String(50), nullable=False)
    password = db.Column(db.String(50), nullable=False)
    reg_date = db.Column(db.String(12), nullable=True)
    email = db.Column(db.String(50), nullable=False)
    role = db.Column(db.String(25), nullable=False)




@app.route('/')
def home():
    posts = Posts.query.filter_by().order_by(Posts.serial_no.desc())[0:2]
    return render_template('index.html', par=parameter, posts=posts)


@app.route('/about')
def about():
    return render_template('about.html', par=parameter)


@app.route('/contact', methods=['GET', 'POST'])
def contact():
    success_email = False
    if (request.method == 'POST'):
        # adding entries to database
        name = request.form.get('name')
        emailFor = request.form.get('email')
        phone = request.form.get('phone')
        message = request.form.get('message')
        entry = Contacts(name=name, phone_num=phone, msg=message, email=emailFor, date=datetime.now())
        db.session.add(entry)
        db.session.commit()
        mail.send_message(
            subject='New message from ' + name,
            sender=emailFor,
            recipients=[parameter['gmail-user']],
            body=message + "\n------------\n" + phone + '\n' + emailFor
        )
        success_email = True

    return render_template('contact.html', par=parameter, success_email=success_email)


@app.route('/post/<string:post_slug>', methods=['GET'])
def post_route(post_slug):
    post = Posts.query.filter_by(slug=post_slug).all()
    return render_template('post.html', par=parameter, post=post[0], amount_of_comments=len(post[0].comments))


@app.route('/dashboard', methods=['GET', 'POST'])
def dashboard():
    if 'user' in session and (session['role'] == 'admin' or session['role'] == 'editor'):
        posts = Posts.query.all()
        users = Users.query.all()
        return render_template('dashboard.html', par=parameter, posts=posts, users=users)

    if request.method == 'POST':
        username = request.form.get('usrname')
        password = request.form.get('pwd')
        if (username == parameter['admin_user'] and password == parameter['admin_pas']):
            session['user'] = username
            posts = Posts.query.all()
            users = Users.query.all()
            return render_template('dashboard.html', par=parameter, posts=posts, users=users)

    # return render_template('login.html', par=parameter)
    return redirect('/')


@app.route('/delete-user/<int:serial_no>', methods=['GET', 'POST'])
def delete_user(serial_no):
    if ('user' in session and session['role'] == 'admin'):
        user = Users.query.filter_by(serial_no=serial_no).first()
        comments = Comments.query.filter_by(login=user.username).all()
        for comment in comments:
            db.session.delete(comment)
        db.session.delete(user)
        db.session.commit()

    return redirect('/dashboard')


@app.route('/change-role/<int:serial_no>/<string:role>', methods=['POST', 'GET'])
def change_role(serial_no, role):
    if ('user' in session and session['role'] == 'admin'):
        user = Users.query.filter_by(serial_no=serial_no).first()
        user.role = role
        db.session.commit()
        message = '<div class="alert alert-success" role="alert">Роль пользователя ' + user.username + ' успешно изменена</div>'
    return redirect('/dashboard')


@app.route("/edit/<string:serial_no>", methods=['GET', 'POST'])
def edit(serial_no):
    if 'user' in session and (session['role'] == 'admin' or session['role'] == 'editor'):
        if request.method == 'POST':
            box_title = request.form.get('title')
            tline = request.form.get('tline')
            slug = request.form.get('slug')
            content = request.form.get('content')
            img_file = request.form.get('img_file')
            author = session['user']
            date = datetime.now()

            ad = AlphabetDetector()
            if len(box_title) < 2 or len(tline) < 2 or len(slug) < 2 or len(content) < 2 or not ad.only_alphabet_chars(slug, "LATIN"):
                post = Posts.query.filter_by(serial_no=serial_no).first()
                message = '<div class="alert alert-warning" role="alert">Вы заполнили не все поля!</div>'
                return render_template('edit.html', par=parameter, serial_no=serial_no, post=post, message=message)

            if serial_no == '0':
                post = Posts(date=date, title=box_title, tagline=tline, slug=slug, content=content, img_file=img_file, author=author)
                db.session.add(post)
                db.session.commit()
            else:
                post = Posts.query.filter_by(serial_no=serial_no).first()
                post.title = box_title
                post.slug = slug
                post.content = content
                post.tagline = tline
                post.img_file = img_file
                post.serial_no = serial_no
                post.date = date
                post.author = author
                db.session.commit()
                return redirect('/dashboard')
        post = Posts.query.filter_by(serial_no=serial_no).first()
        return render_template('edit.html', par=parameter, post=post, serial_no=serial_no, message='')


@app.route('/uploader', methods=['GET', 'POST'])
def uploader():
    if ('user' in session and session['role'] == 'admin'):
        if request.method == 'POST':
            message = ''
            fi = request.files['file1']
            fi.save(os.path.join(app.config['UPLOAD_FOLDER'], secure_filename(fi.filename)))
            message = '<div class="alert alert-success" role="alert">Изображение успешно загружено!</div>'
            posts = Posts.query.all()
            return render_template('dashboard.html', message=message, par=parameter, posts=posts)


@app.route('/logout')
def logout():
    session.pop('user')
    session.pop('auth')
    session.pop('role')
    return redirect('/')


@app.route('/blogs')
def blogs():
    posts = Posts.query.filter_by().all()
    last = math.ceil(len(posts) / int(parameter['no_of_post']))
    page = request.args.get('page')
    if (not str(page).isnumeric()):
        page = 1
    page = int(page)
    posts = posts[(page - 1) * int(parameter['no_of_post']):(page - 1) * int(parameter['no_of_post']) + int(
        parameter['no_of_post'])]
    if page == 1:
        prev = "/blogs"
        next = "/blogs?page=" + str(page + 1)

    elif page == last:
        prev = "/blogs?page=" + str(page - 1)
        next = "/blogs"

    else:
        prev = "/blogs?page=" + str(page - 1)
        next = "/blogs?page=" + str(page + 1)

    return render_template('blogs.html', par=parameter, posts=posts, next=next, prev=prev)


@app.route('/blogs/<string:author>')
def blogs_by_author(author):
    posts = Posts.query.filter_by(author=author).all()
    last = math.ceil(len(posts) / int(parameter['no_of_post']))
    page = request.args.get('page')
    if (not str(page).isnumeric()):
        page = 1
    page = int(page)
    posts = posts[(page - 1) * int(parameter['no_of_post']):(page - 1) * int(parameter['no_of_post']) + int(
        parameter['no_of_post'])]
    if page == 1:
        prev = "/blogs"
        next = "/blogs?page=" + str(page + 1)

    elif page == last:
        prev = "/blogs?page=" + str(page - 1)
        next = "/blogs"

    else:
        prev = "/blogs?page=" + str(page - 1)
        next = "/blogs?page=" + str(page + 1)

    return render_template('blogs.html', par=parameter, posts=posts, next=next, prev=prev)


@app.route("/delete/<string:serial_no>", methods=['GET', 'POST'])
def delete(serial_no):
    if 'user' in session and session['role'] == 'admin':
        post = Posts.query.filter_by(serial_no=serial_no).first()
        comments = Comments.query.filter_by(post_id=serial_no).all()
        for comment in comments:
            db.session.delete(comment)
        db.session.delete(post)
        db.session.commit()
    return redirect('/dashboard')


@app.route("/post/add-comment/<int:serial_no>", methods=['POST'])
def add_comment(serial_no):
    if request.method == 'POST':
        text = request.form.get('text')
        nickname = request.form.get('nickname')
        date = datetime.now()
        post_slug = request.form.get('post.slug')
        if len(nickname) == 0:
            nickname = 'Anonymous'
        post = Comments(text=text, login=nickname, date=date, post_id=serial_no)
        db.session.add(post)
        db.session.commit()
        return redirect('/post/' + post_slug)
    return redirect("/")


@app.route("/login", methods=['GET', 'POST'])
def login_page():
    if request.method == 'POST':
        username = request.form.get('username')
        pwd = request.form.get('password')
        user = Users.query.filter_by(username=username, password=pwd).first()
        if user is None:
            alertbox = '<div class="alert alert-danger" role="alert">Неверный логин или пароль!</div>'
            return render_template('login_template.html', par=parameter, alertbox=alertbox)
        else:
            session['user'] = user.username
            session['auth'] = True
            session['role'] = user.role
            return redirect('/')
    return render_template('login_template.html', par=parameter, alertbox='')


@app.route("/register", methods=['GET', 'POST'])
def register():
    if request.method == 'POST':
        username = request.form.get('username')
        email = request.form.get('email')
        pwd = request.form.get('password')
        pwd2 = request.form.get('password2')
        date = datetime.now()
        if pwd == pwd2:
            if '@' in email:
                if len(username) > 4 and len(pwd) > 4:
                    if Users.query.filter_by(username=username).first() or Users.query.filter_by(email=email).first():
                        alertbox = '<div class="alert alert-danger" role="alert">Пользователь с данным логином или email\'ом уже зарегистрирован!</div>'
                        return render_template('register_template.html', par=parameter, alertbox=alertbox)
                    else:
                        user = Users(username=username, password=pwd, reg_date=date, email=email, role='user')
                        db.session.add(user)
                        db.session.commit()
                        alertbox = '<div class="alert alert-success" role="alert">Вы успешно зарегистрированы!</div>'
                        return render_template('register_template.html', par=parameter, alertbox=alertbox)
                else:
                    alertbox = '<div class="alert alert-danger" role="alert">Логин или пароль короче 5 символов!</div>'
                    return render_template('register_template.html', par=parameter, alertbox=alertbox)
            else:
                alertbox = '<div class="alert alert-danger" role="alert">Email введен не корректно!</div>'
                return render_template('register_template.html', par=parameter, alertbox=alertbox)
        else:
            alertbox = '<div class="alert alert-danger" role="alert">Пароли не совпадают!</div>'
            return render_template('register_template.html', par=parameter, alertbox=alertbox)
    return render_template('register_template.html', par=parameter, alertbox='')


if __name__ == '__main__':
    app.run(debug=True, host='0.0.0.0', port='5000')
